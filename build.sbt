name := "wordSearch"

version := "1.0"

lazy val `wordSearch` = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.6"

evictionWarningOptions in update := EvictionWarningOptions.default
  .withWarnTransitiveEvictions(false)
  .withWarnDirectEvictions(false)

libraryDependencies ++= Seq(cacheApi, ws, specs2 % Test, "com.typesafe.akka" %% "akka-actor" % "2.5.14",
  "com.typesafe.akka" %% "akka-slf4j" % "2.5.14",
  "com.typesafe.akka" %% "akka-testkit" % "2.5.14" % Test, guice,
  "org.scalatest" %% "scalatest" % "3.0.5" % "test")

unmanagedResourceDirectories in Test += baseDirectory(_ / "target/web/public/test").value
