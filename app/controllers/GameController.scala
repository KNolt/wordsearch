package controllers

import akka.actor.ActorSystem
import akka.util.Timeout
import javax.inject.{Inject, Singleton}
import play.api.libs.json.Json
import play.api.mvc._
import services.PuzzleSolver
import services.PuzzleSolver.SolvePuzzle
import services.WordFinder.WordLocation

import scala.concurrent.{ExecutionContext, Future}

@Singleton
class GameController @Inject()(implicit ec: ExecutionContext, system: ActorSystem, val controllerComponents: ControllerComponents) extends BaseController {

  import akka.pattern.ask

  import scala.concurrent.duration._

  implicit val timeout: Timeout = 5.seconds
  private val gameActor = system.actorOf(PuzzleSolver.props(), "game-actor")

  def getGames(boardName: String): Action[AnyContent] = Action.async {
    val future = gameActor ? SolvePuzzle(boardName)
    val map: Future[Seq[WordLocation]] = future.mapTo[Future[Seq[WordLocation]]].flatMap(identity)
    map.map {
      games => Ok(Json.toJson(games))
    }
  }
}
