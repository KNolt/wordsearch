package controllers

import com.google.inject.Inject
import play.api.mvc._

class Application @Inject()(val controllerComponents: ControllerComponents) extends BaseController {

  def index = Action {
    Ok(views.html.index("Your new application is ready."))
  }

}