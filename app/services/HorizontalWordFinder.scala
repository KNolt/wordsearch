package services

import akka.actor.{Actor, Props}
import services.WordFinder.{FindWord, WordLocation}

import scala.annotation.tailrec

object HorizontalWordFinder {
  def props(): Props = Props(new HorizontalWordFinder())

}

class HorizontalWordFinder() extends Actor {

  def receive: Receive = {
    case FindWord(board, word) => findWord(board, word)
  }

  def findWord(board: Array[Array[Char]], word: String): Unit = {
    var validStartingPositions = Array[(Int, Int)]()
    for (x <- board.indices) {
      val indexMatchingFirstLetter = board(x).zipWithIndex.filter(_._1 == word.charAt(0)).map(_._2)
      validStartingPositions = validStartingPositions ++ indexMatchingFirstLetter.map((x, _))
    }
    sender ! checkLocationsForWord(board, word, validStartingPositions, 0)
  }

  //todo convert lines to strings and just see if the string contains the word?
  @tailrec
  private def checkLocationsForWord(board: Array[Array[Char]], word: String, startLocations: Array[(Int, Int)], index: Int): Option[WordLocation] = {
    if (index == startLocations.length) {
      None
    } else {
      var characterPositionsInArray: Array[(Int, Int)] = Array.empty
      val value = startLocations(index)
      val chars = board(value._1)
      val length = word.length
      val startPositionInArray = value._2
      if (startPositionInArray + length <= chars.length) {
        val found = matchWord(word, chars, startPositionInArray, 0, (startingPosition, letterIndex) => startingPosition + letterIndex)
        if (found) {
          characterPositionsInArray = Array.range(startPositionInArray, startPositionInArray + word.length).map((value._1, _))
        }
      }
      //Need to adjust the starting position to account for the zero based array
      val adjustedStartingPosition = startPositionInArray + 1
      if (characterPositionsInArray.isEmpty && adjustedStartingPosition - length >= 0) {
        val foundBackward = matchWord(word, chars, startPositionInArray, 0, (startingPosition, letterIndex) => startingPosition - letterIndex)
        if (foundBackward) {
          characterPositionsInArray = Array.range(adjustedStartingPosition - word.length, adjustedStartingPosition).map((value._1, _))
        }
      }
      if (!characterPositionsInArray.isEmpty) {
        Some(WordLocation(word, characterPositionsInArray))
      } else {
        checkLocationsForWord(board, word, startLocations, index + 1)
      }
    }
  }

  @tailrec
  private def matchWord(word: String, puzzleLine: Array[Char], arrayPosition: Int, letterIndex: Int, f: (Int, Int) => Int): Boolean = {
    if (letterIndex == word.length) {
      true
    } else {
      if (puzzleLine(f(arrayPosition, letterIndex)) != word.charAt(letterIndex)) {
        false
      } else {
        matchWord(word, puzzleLine, arrayPosition, letterIndex + 1, f)
      }
    }
  }

}