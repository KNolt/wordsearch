package services

import akka.actor.{Actor, Props}
import akka.util.Timeout
import play.api.libs.json.{Json, OFormat, OWrites, Reads}
import services.WordFinder.{FindWord, FindWords, WordLocation}

import scala.collection.immutable
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


object WordFinder {
  def props(): Props = Props(new WordFinder())

  implicit val gameReads: Reads[WordLocation] = Json.reads[WordLocation]
  implicit val gameWrites: OWrites[WordLocation] = Json.writes[WordLocation]
  implicit val gameFormat: OFormat[WordLocation] = Json.format[WordLocation]

  case class FindWord(board: Array[Array[Char]], word: String)

  case class WordLocation(word: String, characterLocations: Array[(Int, Int)])
  case class FindWords(board: Array[Array[Char]], words: List[String])

}


class WordFinder() extends Actor {

  import akka.pattern.ask

  import scala.concurrent.duration._

  implicit val timeout: Timeout = 20.seconds
  private val horizontalWordFinder = context.actorOf(HorizontalWordFinder.props(), "horizontal-actor")
  private val verticalWordFinder = context.actorOf(VerticalWordFinder.props(), "vertical-actor")


  def receive: Receive = {
    case FindWords(board, words) => findWords(board, words)
  }

  def findWords(board: Array[Array[Char]], words: List[String]): Unit = {
    val horizontalWords: immutable.Seq[Future[Option[WordLocation]]] = words.map(horizontalWordFinder ? FindWord(board, _)).map(_.mapTo[Option[WordLocation]])
    val verticalWords: immutable.Seq[Future[Option[WordLocation]]] = words.map(verticalWordFinder ? FindWord(board, _)).map(_.mapTo[Option[WordLocation]])
    val eventualMaybeLocations: Future[immutable.Seq[WordLocation]] = Future.sequence(horizontalWords ++ verticalWords).map(_.filter(_.nonEmpty)).map(_.map(_.get))
    sender() ! eventualMaybeLocations
  }

}