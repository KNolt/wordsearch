package services

import akka.actor.{Actor, Props}
import akka.util.Timeout
import services.PuzzleSolver.SolvePuzzle
import services.WordFinder.{FindWords, WordLocation}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

object PuzzleSolver {
  def props(): Props = Props(new PuzzleSolver())

  case class SolvePuzzle(boardName: String)

}

class PuzzleSolver() extends Actor {

  import akka.pattern.ask

  import ExecutionContext.Implicits.global
  import scala.concurrent.duration._
  import scala.io._

  implicit val timeout: Timeout = 5.seconds
  private val wordFinder = context.actorOf(WordFinder.props(), "wordFinder-actor")

  def receive: Receive = {
    case SolvePuzzle(board) => solvePuzzle(board)
  }

  def solvePuzzle(board: String): Unit = {
    val array = Source.fromInputStream(PuzzleSolver.getClass.getResourceAsStream(s"/$board.txt"))
      .getLines()
      .map(_.split(" ").map(_.charAt(0)))
      .toArray
    val list = Source.fromInputStream(PuzzleSolver.getClass.getResourceAsStream(s"/${board}Words.txt"))
      .getLines()
      .flatMap(_.split(" ").map(_.trim))
      .toList
    val future = wordFinder ? FindWords(array, list)
    val map: Future[Seq[WordLocation]] = future.mapTo[Future[Seq[WordLocation]]].flatMap(identity)
    sender() ! map
  }


  def loan[A <: AutoCloseable, B](resource: A)(block: A => B): B = {
    Try(block(resource)) match {
      case Success(result) =>
        resource.close()
        result
      case Failure(e) =>
        resource.close()
        throw e
    }
  }

}