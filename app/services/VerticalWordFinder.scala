package services

import akka.actor.{Actor, Props}
import services.WordFinder.{FindWord, WordLocation}

import scala.annotation.tailrec

object VerticalWordFinder {
  def props(): Props = Props(new VerticalWordFinder())
}


class VerticalWordFinder() extends Actor {

  def receive: Receive = {
    case FindWord(board, word) => findWord(board, word)
  }

  def findWord(board: Array[Array[Char]], word: String): Unit = {
    var validStartingPositions = Array[(Int, Int)]()
    for (x <- board.indices) {
      val indexMatchingFirstLetter = board(x).zipWithIndex.filter(_._1 == word.charAt(0)).map(_._2)
      validStartingPositions = validStartingPositions ++ indexMatchingFirstLetter.map((x, _))
    }
    sender ! checkLocationsForWord(board, word, validStartingPositions, 0)
  }

  @tailrec
  private def checkLocationsForWord(board: Array[Array[Char]], word: String, startLocations: Array[(Int, Int)], index: Int): Option[WordLocation] = {
    if (index == startLocations.length) {
      None
    } else {
      var characterPositionsInArray: Array[(Int, Int)] = Array.empty
      val value = startLocations(index)
      val transpose = board.transpose
      val chars = transpose(value._2)
      val wordLength = word.length
      val startPositionInArray = value._1
      if (startPositionInArray + wordLength <= chars.length) {
        val found = matchWord(word, chars, startPositionInArray, 0, (startingPosition, letterIndex) => startingPosition + letterIndex)
        if (found) {
          characterPositionsInArray = Array.range(startPositionInArray, startPositionInArray + word.length).map((_, value._2))
        }
      }
      //Need to adjust the starting position to account for the zero based array
      val adjustedStartingPosition = startPositionInArray + 1
      if (characterPositionsInArray.isEmpty && adjustedStartingPosition - wordLength >= 0) {
        val foundBackward = matchWord(word, chars, startPositionInArray, 0, (startingPosition, letterIndex) => startingPosition - letterIndex)
        if (foundBackward) {
          characterPositionsInArray = Array.range(adjustedStartingPosition - wordLength, adjustedStartingPosition).map((_, value._2))
        }
      }
      if (!characterPositionsInArray.isEmpty) {
        Some(WordLocation(word, characterPositionsInArray))
      } else {
        checkLocationsForWord(board, word, startLocations, index + 1)
      }
    }
  }

  @tailrec
  private def matchWord(word: String, puzzleLine: Array[Char], arrayPosition: Int, letterIndex: Int, f: (Int, Int) => Int): Boolean = {
    if (letterIndex == word.length) {
      true
    } else {
      if (puzzleLine(f(arrayPosition, letterIndex)) != word.charAt(letterIndex)) {
        false
      } else {
        matchWord(word, puzzleLine, arrayPosition, letterIndex + 1, f)
      }
    }
  }

}