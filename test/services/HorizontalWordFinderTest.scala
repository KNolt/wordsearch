package services

import akka.actor.ActorSystem
import akka.pattern.ask
import akka.testkit.{ImplicitSender, TestActorRef, TestKit}
import akka.util.Timeout
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import services.WordFinder.{FindWord, WordLocation}

import scala.concurrent.duration._
import scala.io.Source
import scala.util.Success

class HorizontalWordFinderTest extends TestKit(ActorSystem("MySpec")) with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll {


  implicit val timeout: Timeout = 5.seconds

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  "An HorizontalWordFinder actor" must {

    "find words spelled normally on the same line in an array of characters" in {
      val array = Source.fromInputStream(PuzzleSolver.getClass.getResourceAsStream("/forwardPuzzle.txt"))
        .getLines()
        .map(_.split(" ").map(_.charAt(0)))
        .toArray
      val list = Source.fromInputStream(PuzzleSolver.getClass.getResourceAsStream("/forwardPuzzleWords.txt"))
        .getLines()
        .flatMap(_.split(" ").map(_.trim))
        .toList

      val actorRef = TestActorRef(HorizontalWordFinder.props())
      val futures = list.map(actorRef ? FindWord(array, _))
      for (elem <- futures) {
        assert(elem.value.asInstanceOf[Some[Success[Option[WordLocation]]]].value.asInstanceOf[Success[Option[WordLocation]]].value.asInstanceOf[Option[WordLocation]].isDefined)
      }
    }
  }

  "An HorizontalWordFinder actor" must {

    "find words spelled backwards on the same line in an array of characters" in {
      val array = Source.fromInputStream(PuzzleSolver.getClass.getResourceAsStream("/backwardPuzzle.txt"))
        .getLines()
        .map(_.split(" ").map(_.charAt(0)))
        .toArray
      val list = Source.fromInputStream(PuzzleSolver.getClass.getResourceAsStream("/backwardPuzzleWords.txt"))
        .getLines()
        .flatMap(_.split(" ").map(_.trim))
        .toList

      val actorRef = TestActorRef(HorizontalWordFinder.props())
      val futures = list.map(actorRef ? FindWord(array, _))
      for (elem <- futures) {
        assert(elem.value.asInstanceOf[Some[Success[Option[WordLocation]]]].value.asInstanceOf[Success[Option[WordLocation]]].value.asInstanceOf[Option[WordLocation]].isDefined)
      }
    }
  }
}
