package services

import akka.actor.ActorSystem
import akka.pattern.ask
import akka.testkit.{ImplicitSender, TestActorRef, TestKit}
import akka.util.Timeout
import org.scalatest.{BeforeAndAfterAll, Matchers, WordSpecLike}
import services.WordFinder.{FindWord, WordLocation}

import scala.concurrent.duration._
import scala.io.Source
import scala.util.Success

class VerticalWordFinderTest extends TestKit(ActorSystem("MySpec")) with ImplicitSender
  with WordSpecLike with Matchers with BeforeAndAfterAll {


  implicit val timeout: Timeout = 5.seconds

  override def afterAll {
    TestKit.shutdownActorSystem(system)
  }

  "An VerticalWordFinder actor" must {

    "find words spelled normally but on a vertical line in an array of characters" in {
      val array = Source.fromInputStream(PuzzleSolver.getClass.getResourceAsStream("/DownPuzzle.txt"))
        .getLines()
        .map(_.split(" ").map(_.charAt(0)))
        .toArray
      val list = Source.fromInputStream(PuzzleSolver.getClass.getResourceAsStream("/DownPuzzleWords.txt"))
        .getLines()
        .flatMap(_.split(" ").map(_.trim))
        .toList

      val actorRef = TestActorRef(VerticalWordFinder.props())
      val futures = list.map(actorRef ? FindWord(array, _))
      for (elem <- futures) {
        assert(elem.value.asInstanceOf[Some[Success[Option[WordLocation]]]].value.asInstanceOf[Success[Option[WordLocation]]].value.asInstanceOf[Option[WordLocation]].isDefined)
      }
    }
  }

  "An VerticalWordFinder actor" must {

    "find words spelled backwards on the a vertical line in an array of characters" in {
      val array = Source.fromInputStream(PuzzleSolver.getClass.getResourceAsStream("/UpPuzzle.txt"))
        .getLines()
        .map(_.split(" ").map(_.charAt(0)))
        .toArray
      val list = Source.fromInputStream(PuzzleSolver.getClass.getResourceAsStream("/UpPuzzleWords.txt"))
        .getLines()
        .flatMap(_.split(" ").map(_.trim))
        .toList

      val actorRef = TestActorRef(VerticalWordFinder.props())
      val futures = list.map(actorRef ? FindWord(array, _))
      for (elem <- futures) {
        assert(elem.value.asInstanceOf[Some[Success[Option[WordLocation]]]].value.asInstanceOf[Success[Option[WordLocation]]].value.asInstanceOf[Option[WordLocation]].isDefined)
      }
    }
  }
}
